export interface Brief {
  title: string;
  comment: string;
  productId: string;
}
