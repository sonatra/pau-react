export enum FetchingStatus {
  "NULL" = "NULL",
  "PENDING" = "PENDING",
  "SUCCESS" = "SUCCESS",
  "FAILED" = "FAILED"
}
