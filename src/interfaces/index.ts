import { Brief } from "../models/briefs";
import { Product } from "../models/products";

export interface BriefWithProductName extends Brief {
  productName: Product["name"];
}
