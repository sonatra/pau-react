import { ThunkAction } from "redux-thunk";
import axios from "axios";
import { ActionCreator, AnyAction } from "redux";
import { FetchingStatus } from "../utils/fetchingStatus";
import { Product } from "../models/products";
import { RootState } from "../reducers";

export enum ProductsActionTypes {
  "FETCH_PRODUCTS" = "@@products fetch products",
  "SELECT_PRODUCT" = "@products select product",
  "FETCH_PRODUCTS_STATUS" = "@@products fetch products status"
}

interface FetchProductStatus {
  type: ProductsActionTypes.FETCH_PRODUCTS_STATUS;
  payload: { status: FetchingStatus; products?: Product[] };
}
export const fetchProductsStatus: ActionCreator<FetchProductStatus> = (
  status,
  products
) => ({
  type: ProductsActionTypes.FETCH_PRODUCTS_STATUS,
  payload: { status, products }
});

interface SelectProduct {
  type: ProductsActionTypes.SELECT_PRODUCT;
  payload: { id: string };
}
export const selectProduct: ActionCreator<SelectProduct> = id => ({
  type: ProductsActionTypes.SELECT_PRODUCT,
  payload: { id }
});

interface FetchProduct {
  type: ProductsActionTypes.FETCH_PRODUCTS;
}
export const fetchProducs: ActionCreator<
  ThunkAction<Promise<void>, RootState, null, AnyAction>
> = () => {
  return async dispatch => {
    try {
      dispatch(fetchProductsStatus(FetchingStatus.PENDING));
      const { data: products } = await axios.get<Product[]>(
        "http://localhost:3001/products"
      );
      setTimeout(() => {
        dispatch(fetchProductsStatus(FetchingStatus.SUCCESS, products));
      }, 800);
    } catch (err) {
      dispatch(fetchProductsStatus(FetchingStatus.FAILED));
    }
  };
};

export type ProductActions = FetchProduct | SelectProduct | FetchProductStatus;
