import { ThunkAction } from "redux-thunk";
import axios from "axios";
import { ActionCreator, AnyAction } from "redux";
import { FetchingStatus } from "../utils/fetchingStatus";
import { Brief } from "../models/briefs";
import { RootState } from "../reducers";

export enum BriefsActionTypes {
  "FETCH_BRIEFS" = "@@briefs fetch briefs",
  "FETCH_BRIEFS_STATUS" = "@@briefs fetch briefs status",
  "CREATE_BRIEF" = "@@briefs create a brief",
  "CREATE_BRIEF_STATUS" = "@@briefs create a brief status"
}

interface FetchBriefStatus {
  type: BriefsActionTypes.FETCH_BRIEFS_STATUS;
  payload: { status: FetchingStatus; briefs?: Brief[] };
}
export const fetchBriefsStatus: ActionCreator<FetchBriefStatus> = (
  status,
  briefs
) => ({
  type: BriefsActionTypes.FETCH_BRIEFS_STATUS,
  payload: { status, briefs }
});

interface FetchBrief {
  type: BriefsActionTypes.FETCH_BRIEFS;
}
export const fetchBriefs: ActionCreator<
  ThunkAction<Promise<void>, RootState, null, AnyAction>
> = () => {
  return async dispatch => {
    try {
      dispatch(fetchBriefsStatus(FetchingStatus.PENDING));
      const { data: briefs } = await axios.get<Brief[]>(
        "http://localhost:3001/briefs"
      );
      setTimeout(() => {
        dispatch(fetchBriefsStatus(FetchingStatus.SUCCESS, briefs));
      }, 500);
    } catch (err) {
      dispatch(fetchBriefsStatus(FetchingStatus.FAILED));
    }
  };
};

interface CreateBriefStatus {
  type: BriefsActionTypes.CREATE_BRIEF_STATUS;
  payload: { status: FetchingStatus; brief?: Brief };
}
export const createBriefsStatus: ActionCreator<CreateBriefStatus> = (
  status,
  brief
) => ({
  type: BriefsActionTypes.CREATE_BRIEF_STATUS,
  payload: { status, brief }
});

interface CreateBrief {
  type: BriefsActionTypes.CREATE_BRIEF;
}

export const CreateBrief: ActionCreator<
  ThunkAction<Promise<void>, RootState, null, AnyAction>
> = (brief: Brief) => async dispatch => {
  try {
    dispatch(createBriefsStatus(FetchingStatus.PENDING));
    await axios.post<Brief[]>("http://localhost:3001/briefs", brief);
    setTimeout(() => {
      dispatch(createBriefsStatus(FetchingStatus.SUCCESS, brief));
    }, 500);
  } catch (err) {
    dispatch(createBriefsStatus(FetchingStatus.FAILED));
  }
};

export type BriefActions =
  | FetchBrief
  | FetchBriefStatus
  | CreateBrief
  | CreateBriefStatus;
