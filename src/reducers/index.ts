import { combineReducers } from "redux";
import { ProductReducer } from "./products";
import { BriefReducer } from "./briefs";

export const RootReducer = combineReducers({
  product: ProductReducer,
  brief: BriefReducer
});

export type RootState = ReturnType<typeof RootReducer>;
