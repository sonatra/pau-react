import { Product } from "../models/products";
import { ProductActions, ProductsActionTypes } from "../actions/products";
import { FetchingStatus } from "../utils/fetchingStatus";

export interface ProductState {
  products: Product[];
  selectedProduct: string | null;
  fetchProductStatus: FetchingStatus;
}

const ProductState: ProductState = {
  selectedProduct: null,
  products: [],
  fetchProductStatus: FetchingStatus.NULL
};

export const ProductReducer = (
  state = ProductState,
  action: ProductActions
): ProductState => {
  switch (action.type) {
    case ProductsActionTypes.FETCH_PRODUCTS_STATUS:
      return {
        ...state,
        fetchProductStatus: action.payload.status,
        products: action.payload.products || []
      };
    case ProductsActionTypes.SELECT_PRODUCT:
      return {
        ...state,
        selectedProduct: action.payload.id
      };

    default:
      return state;
  }
};
