import { Brief } from "../models/briefs";
import { BriefActions, BriefsActionTypes } from "../actions/briefs";
import { FetchingStatus } from "../utils/fetchingStatus";

export interface BriefState {
  briefs: Brief[];
  fetchBriefStatus: FetchingStatus;
  createBriefStatus: FetchingStatus;
}

const BriefState: BriefState = {
  briefs: [],
  fetchBriefStatus: FetchingStatus.NULL,
  createBriefStatus: FetchingStatus.NULL
};

export const BriefReducer = (
  state = BriefState,
  action: BriefActions
): BriefState => {
  switch (action.type) {
    case BriefsActionTypes.FETCH_BRIEFS_STATUS:
      return {
        ...state,
        fetchBriefStatus: action.payload.status,
        briefs: action.payload.briefs || []
      };
    case BriefsActionTypes.CREATE_BRIEF_STATUS:
      const { brief, status } = action.payload;
      return {
        ...state,
        createBriefStatus: status,
        briefs:
          status === FetchingStatus.SUCCESS && brief
            ? [brief, ...state.briefs]
            : state.briefs
      };

    default:
      return state;
  }
};
