import React from "react";
import { BriefState } from "../reducers/briefs";
import { RootState } from "../reducers";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { fetchBriefs } from "../actions/briefs";
import { connect } from "react-redux";
import { FetchingStatus } from "../utils/fetchingStatus";
import { Brief } from "./Brief";
import { List, Typography, Divider } from "@material-ui/core";
import { Loader } from "./common/Loader";
import { getBriefsWithProductName } from "../selectors/briefList";
import { BriefWithProductName } from "../interfaces";

interface BriefListProps {}

interface DispatchMethods {
  fetchBriefs: () => Promise<void>;
}

interface StateProps {
  briefs: BriefWithProductName[];
  fetchBriefStatus: BriefState["fetchBriefStatus"];
}

interface State {}

type Props = BriefListProps & StateProps & DispatchMethods;

export class BriefListComponent extends React.Component<Props, State> {
  componentDidMount() {
    const { fetchBriefs } = this.props;
    fetchBriefs();
  }

  render() {
    const { briefs, fetchBriefStatus } = this.props;
    if (fetchBriefStatus === FetchingStatus.PENDING) {
      return <Loader text="Loading Briefs ..." />;
    }
    return (
      <List>
        {briefs.length > 0 ? (
          briefs.map((brief, i) => (
            <React.Fragment key={`${i}-${brief.title}`}>
              <Brief {...brief} />
              <Divider />
            </React.Fragment>
          ))
        ) : (
          <Typography>No briefs at the moment ...</Typography>
        )}
      </List>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  briefs: getBriefsWithProductName(state),
  fetchBriefStatus: state.brief.fetchBriefStatus
});

const mapDispatchToProps = (
  dispatch: ThunkDispatch<RootState, null, AnyAction>
) => ({
  fetchBriefs: () => dispatch(fetchBriefs())
});

export const BriefList = connect(
  mapStateToProps,
  mapDispatchToProps
)(BriefListComponent);
