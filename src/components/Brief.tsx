import React from "react";
import {
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Typography
} from "@material-ui/core";
import WorkIcon from "@material-ui/icons/Work";
import { BriefWithProductName } from "../interfaces";

export const Brief: React.FC<BriefWithProductName> = ({
  title,
  comment,
  productName
}) => {
  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar>
          <WorkIcon />
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={title}
        disableTypography
        secondary={
          <>
            <Typography color="textSecondary">Comment: {comment}</Typography>
            <Typography color="textSecondary">
              Product: {productName || "unknown"}
            </Typography>
          </>
        }
      />
    </ListItem>
  );
};
