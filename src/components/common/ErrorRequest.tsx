import React from "react";
import { Grid, Typography } from "@material-ui/core";
import ErrorIcon from "@material-ui/icons/ErrorOutline";

export const ErrorRequest: React.FC<{ text: string }> = ({ text }) => (
  <Grid container justify="center" alignItems="center">
    <Grid item>
      <ErrorIcon fontSize="large" color="error" />
    </Grid>
    <Grid item>
      <Typography color="error">{text}</Typography>
    </Grid>
  </Grid>
);
