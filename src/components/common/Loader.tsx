import React from "react";
import { Grid, CircularProgress, Typography } from "@material-ui/core";

export const Loader: React.FC<{ text: string }> = ({ text }) => (
  <Grid container spacing={1}>
    <Grid item>
      <CircularProgress color="primary" size={20} />
    </Grid>
    <Grid item>
      <Typography color="textSecondary" style={{ display: "inline-block" }}>
        {text}
      </Typography>
    </Grid>
  </Grid>
);
