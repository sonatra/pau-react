import React, { FormEvent, ChangeEvent } from "react";
import { ProductState } from "../reducers/products";
import { connect } from "react-redux";
import { RootState } from "../reducers";
import { fetchProducs } from "../actions/products";
import { FetchingStatus } from "../utils/fetchingStatus";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { CreateBrief } from "../actions/briefs";
import { Brief } from "../models/briefs";
import { BriefState } from "../reducers/briefs";
import { TextField, MenuItem, Button } from "@material-ui/core";
import { Loader } from "./common/Loader";
import { ErrorRequest } from "./common/ErrorRequest";

interface BriefCreationFormProps {}

interface DispatchMethods {
  fetchProducts: () => Promise<void>;
  createBrief: (brief: Brief) => Promise<void>;
}

interface StateProps {
  products: ProductState["products"];
  fetchProductsStatus: ProductState["fetchProductStatus"];
  createBriefsStatus: BriefState["createBriefStatus"];
}

interface State {
  form: Brief;
}

type Props = BriefCreationFormProps & StateProps & DispatchMethods;

class BriefCreationFormComponent extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { form: { title: "", comment: "", productId: "" } };
  }

  componentDidMount() {
    const { fetchProducts } = this.props;
    fetchProducts();
  }

  onFormChange = (name: string) => (e: ChangeEvent<any>) => {
    const value = e.target.value;
    this.setState(prevState => ({
      ...prevState,
      form: { ...prevState.form, [name]: value }
    }));
  };

  onCreateBrief = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { createBrief } = this.props;
    const { form } = this.state;
    createBrief(form);
  };

  render() {
    const { products, fetchProductsStatus, createBriefsStatus } = this.props;
    const { title, comment, productId } = this.state.form;
    if (fetchProductsStatus === FetchingStatus.PENDING) {
      return <Loader text="Loading products ..." />;
    }
    if (fetchProductsStatus === FetchingStatus.FAILED) {
      return (
        <ErrorRequest text="You must start the fake server by running command `npm start:server` !" />
      );
    }

    return (
      <form
        onSubmit={this.onCreateBrief}
        style={{
          width: 200,
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <TextField
          required
          id="briefTitle"
          label="Title"
          value={title}
          onChange={this.onFormChange("title")}
          margin="normal"
          variant="outlined"
        />
        <TextField
          required
          id="briefComment"
          label="Comment"
          value={comment}
          onChange={this.onFormChange("comment")}
          margin="normal"
          variant="outlined"
        />

        <TextField
          id="outlined-select-currency"
          style={{ width: "100%" }}
          select
          label="Select"
          value={productId}
          onChange={this.onFormChange("productId")}
          margin="normal"
          variant="outlined"
        >
          {products.map(product => (
            <MenuItem key={product.id} value={product.id}>
              {product.name}
            </MenuItem>
          ))}
        </TextField>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          type="submit"
          disabled={createBriefsStatus === FetchingStatus.PENDING}
        >
          Create a brief
        </Button>
      </form>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  products: state.product.products,
  fetchProductsStatus: state.product.fetchProductStatus,
  fetchBriefsStatus: state.brief.fetchBriefStatus,
  createBriefsStatus: state.brief.createBriefStatus
});

const mapDispatchToProps = (
  dispatch: ThunkDispatch<RootState, null, AnyAction>
) => ({
  fetchProducts: () => dispatch(fetchProducs()),
  createBrief: (brief: Brief) => dispatch(CreateBrief(brief))
});

export const BriefCreationForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(BriefCreationFormComponent);
