import { RootState } from "../reducers";
import { createSelector } from "reselect";

const getBriefs = (state: RootState) => state.brief.briefs;
const getProducts = (state: RootState) => state.product.products;

export const getBriefsWithProductName = createSelector(
  [getBriefs, getProducts],
  (briefs, products) =>
    briefs.map(brief => {
      const product = products.find(product => product.id === brief.productId);
      return {
        ...brief,
        productName: product ? product.name : "No Product associated"
      };
    })
);
