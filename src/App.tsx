import React from "react";
import "./App.css";
import { BriefCreationForm } from "./components/BriefForm";
import { BriefList } from "./components/BriefList";
import { Box, Grid } from "@material-ui/core";

interface AppProps {}
interface StateProps {}

type Props = AppProps & StateProps;

export const App: React.FC<Props> = () => {
  return (
    <Box p={8}>
      <Grid container justify="center">
        <Grid item xs={4}>
          <BriefCreationForm />
        </Grid>
        <Grid item xs={8}>
          <BriefList />
        </Grid>
      </Grid>
    </Box>
  );
};
